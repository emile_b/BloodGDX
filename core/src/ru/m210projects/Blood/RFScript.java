// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Build.Strhandler.toLowerCase;
import static ru.m210projects.Build.OnSceenDisplay.Console.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ru.m210projects.Blood.Types.SFX;
import ru.m210projects.Build.FileHandle.Group;
import ru.m210projects.Build.OnSceenDisplay.Console;
import ru.m210projects.Build.Script.Scriptfile;

public class RFScript {

	private static final int T_DATA = 0;
	private static final int T_RESOURCE = 1;
	private static final int T_AS = 2;
	private static final int T_SYMBOL = 3;
	private static final int T_FILE = 4;
	private static final int T_CDTRACK = 5;
	private static final int T_ERROR = 6;
	private static final int T_EOF = 7;
	
	public static final int MAXUSERTRACKS = 64;
	public static String usertrack[] = new String[MAXUSERTRACKS];

	private static HashMap<String, Integer> fileids = new HashMap<String, Integer>();
	private static HashMap<String, Integer> fileflags = new HashMap<String, Integer>();

	private static Integer getValue(Scriptfile sf) {
		String txt = sf.getstring();
		txt = txt.replaceAll("[^0-9x-]", "");

		int hexIndex = txt.indexOf("0x");
		if (hexIndex != -1)
			txt = txt.substring(hexIndex + 2);

		try {
			return Integer.parseInt(txt, (hexIndex != -1) ? 16 : 10);
		} catch (Exception e) {
			return null;
		}
	}

	private static int geteol(Scriptfile sf) {
		int start = sf.textptr;
		while ((sf.textptr < sf.eof) && (sf.textbuf.charAt(sf.textptr)) != ';')
			sf.textptr++;

		int end = sf.textptr + 1;
		sf.textptr = start;
		return end;
	}

	public static int rfsfileid(String filename) {
		if (fileids.size() > 0) {
			Integer out = fileids.get(filename);
			if (out != null)
				return out;
		}
		return -1;
	}

	public static int rfsfileflag(String filename) {
		if (fileflags.size() > 0) {
			Integer out = fileflags.get(filename);
			if (out != null)
				return out;
		}
		return 0;
	}

	public static void rfsclean() {
		fileids.clear();
		fileflags.clear();
	}

	private static void rfspreparse(Scriptfile sf, byte[] data, int flen) {
		// Count number of lines
		int numcr = 1;
		for (int i = 0; i < flen; i++) {
			// detect all 4 types of carriage return (\r, \n, \r\n, \n\r :)
			int cr = 0;
			if (data[i] == '\r') {
				i += ((data[i + 1] == '\n') ? 1 : 0);
				cr = 1;
			} else if (data[i] == '\n') {
				i += ((data[i + 1] == '\r') ? 1 : 0);
				cr = 1;
			}
			if (cr != 0) {
				numcr++;
				continue;
			}
		}

		sf.linenum = numcr;
		sf.lineoffs = new int[numcr];

		// Preprocess file for comments (// and /*...*/, and convert all whitespace to
		// single spaces)
		int nflen = 0, space = 0, cs = 0, inquote = 0;
		numcr = 0;
		for (int i = 0; i < flen; i++) {
			// detect all 4 types of carriage return (\r, \n, \r\n, \n\r :)
			int cr = 0;
			if (data[i] == '\r') {
				i += ((data[i + 1] == '\n') ? 1 : 0);
				cr = 1;
			} else if (data[i] == '\n') {
				i += ((data[i + 1] == '\r') ? 1 : 0);
				cr = 1;
			}
			if (cr != 0) {
				// Remember line numbers by storing the byte index at the start of each line
				// Line numbers can be retrieved by doing a binary search on the byte index :)
				sf.lineoffs[numcr++] = nflen;
				inquote = 0; // Blood RFS
				if (cs == 1)
					cs = 0;
				space = 1;
				continue; // strip CR/LF
			}

			if ((inquote == 0) && ((data[i] == ' ') || (data[i] == '\t'))) {
				space = 1;
				continue;
			} // strip Space/Tab
			if ((data[i] == '/') && (data[i + 1] == '/') && (cs == 0))
				cs = 1;
			if ((data[i] == '\\') && (data[i + 1] == '\\') && (cs == 0))
				cs = 1;
			if ((data[i] == '/') && (data[i + 1] == '*') && (cs == 0)) {
				space = 1;
				cs = 2;
			}
			if ((data[i] == '*') && (data[i + 1] == '/') && (cs == 2)) {
				cs = 0;
				i++;
				continue;
			}

			if (cs != 0)
				continue;

			if (space != 0) {
				data[nflen++] = 0;
				space = 0;
			}

			if ((data[i] == '\\') && (data[i + 1] == '\"')) {
				i++;
				data[nflen++] = '\"';
				continue;
			}
			if (data[i] == '\"') {
				inquote ^= 1;
				continue;
			}

			data[nflen++] = data[i];
		}
		data[nflen++] = 0;
		sf.lineoffs[numcr] = nflen;
		data[nflen++] = 0;

		flen = nflen;

		sf.textbuf = new String(data);
		sf.textptr = 0;
		sf.eof = nflen - 1;
	}

	public static Scriptfile getRFS(byte[] buf) {
		Scriptfile sf = new Scriptfile();
		int flen = buf.length;
		byte[] data = Arrays.copyOf(buf, flen + 2);

		rfspreparse(sf, data, flen);

		return sf;
	}

	public static void nextLine(Scriptfile sf, int line) {
		if (line < sf.linenum)
			sf.textptr = sf.lineoffs[line - 1];
	}
	
	private static final Map<String, Integer> basetokens = new HashMap<String, Integer>() {
		private static final long serialVersionUID = 1L;
		{
			put("data", T_DATA);
			put("resource", T_RESOURCE);
			put("as", T_AS);
			put(";", T_SYMBOL); // end of file (error issue)
			put("file", T_FILE); // cue cd file
			put("track", T_CDTRACK); // cd track
		}
	};
	
	private static int gettoken(Scriptfile sf, Map<String , Integer> list) {
		int tok;
		if (sf == null) return T_ERROR;
		if ((tok = sf.gettoken()) == -2) 
			return T_EOF;

		Integer out = list.get(toLowerCase(sf.textbuf.substring(tok, sf.textptr)));
		if (out != null)
			return out;

		sf.errorptr = sf.textptr;
		return T_ERROR;
	}

	public static void parserfs(Group group, String name, byte[] data) {
		Scriptfile sf = getRFS(data);
		sf.filename = name;

		while (!sf.eof()) {
			int tokn = gettoken(sf, basetokens);
			int line = sf.getlinum(sf.textptr);
			switch (tokn) {
			case T_ERROR:
				Console.Println("Error on line " + sf.filename + ":" + line + ", skipping...", OSDTEXT_RED);
				nextLine(sf, line);
				break;
			case T_EOF:
			case T_SYMBOL:
				break;
			case T_FILE:
				String muspath = sf.getstring();
				sf.getstring(); // header type
				int mustoken = gettoken(sf, basetokens);
				if (mustoken == T_CDTRACK) {
					int nTrack = getValue(sf);
					if (nTrack > 1) // ignore .bin
						usertrack[nTrack] = game.getFilename(muspath);
					sf.getstring(); // file type
					sf.getstring(); // index
					sf.getstring(); // index num
					sf.getstring(); // time
				}
				break;
			case T_DATA:
				String sfxpath = sf.getstring();
				int dattoken = gettoken(sf, basetokens);
				if (dattoken == T_AS) {
					try {
						int soundId = getValue(sf);
						int relVol = getValue(sf);
						int pitch = getValue(sf);
						int pitchrange = getValue(sf);
						int format = getValue(sf);
						int loopStart = getValue(sf);

						String rawName = sf.getstring();
						int index = rawName.indexOf(";"); // end of the line
						if (index != -1)
							rawName = rawName.substring(0, index);
						pSFXs[soundId] = new SFX(relVol, pitch, pitchrange, format, loopStart, rawName);

						String idname = rawName;
						if (idname.indexOf(".") == -1)
							idname = idname + ".raw";
						fileids.put(idname, soundId);

						if (group != null)
							group.add(sfxpath, pSFXs[soundId].getBytes(), soundId);

					} catch (Exception e) {
						Console.Println("Error on line " + sf.filename + ":" + line + ", skipping...", OSDTEXT_RED);
						nextLine(sf, line);
					}
				} else {
					Console.Println("Error on line " + sf.filename + ":" + line + ", skipping...", OSDTEXT_RED);
					nextLine(sf, line);
				}
				break;
			case T_RESOURCE:
				int eol = geteol(sf);
				String path = game.getFilename(sf.getstring());
				if (path != null) {
					int optr = sf.textptr;
					int restoken = gettoken(sf, basetokens);
					String filename = toLowerCase(path);
					filename = filename.replaceAll("[,;]", "");
					if (restoken == T_AS) {
						Integer value = null;
						if ((value = getValue(sf)) != null) {
							int fileid = value;
							fileids.put(filename, fileid);
						}
					} else
						sf.textptr = optr;

					if (sf.textptr < eol) {
						String flag = sf.getstring();
						if (flag.startsWith("preload"))
							fileflags.put(filename, 4);
						else if (flag.startsWith("prelock"))
							fileflags.put(filename, 8);
					}
					nextLine(sf, line);
					break;
				}
				break;
			}
		}
	}
}
