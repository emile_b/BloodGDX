// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import static ru.m210projects.Blood.Globals.gFullMap;
import static ru.m210projects.Blood.Globals.gMapScrollMode;
import static ru.m210projects.Blood.Globals.gPlayer;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.View.gViewIndex;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.Net.Mmulti.connecthead;
import static ru.m210projects.Build.Net.Mmulti.connectpoint2;

import java.util.Arrays;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Polymost;

public class BloodPolymost extends Polymost {

	private int sprites[] = new int[MAXPLAYERS];
	
	public BloodPolymost(Engine engine) {
		super(engine);
		polymost2d.initmap(24, 24, 0, false, false, false);
	}

	@Override
	public void drawoverheadmap(int cposx, int cposy, int czoom, short cang) {
		Arrays.fill(sprites, -1);
		for (int i = connecthead; i >= 0; i = connectpoint2[i]) 
			sprites[i] = gPlayer[i].nSprite;
		polymost2d.setmapsettings(gFullMap, pGameInfo.nGameType == 1, gMapScrollMode, gViewIndex, sprites);
		polymost2d.drawoverheadmap(cposx, cposy, czoom, cang);
	}

}
