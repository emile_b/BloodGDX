// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Build.Engine.*;

import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import ru.m210projects.Blood.Types.PICANM;
import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.FileHandle.DataResource;
import ru.m210projects.Build.FileHandle.Group;
import ru.m210projects.Build.FileHandle.GroupResource;
import ru.m210projects.Build.FileHandle.Resource.ResourceData;
import ru.m210projects.Build.Loader.Voxels.KVXLoader;
import ru.m210projects.Build.Loader.Voxels.Voxel;
import ru.m210projects.Build.Script.DefScript;

public class Tile {
	
//	public static final int kHUDLeft = kUserTiles;
//	public static final int kHUDRight = kUserTiles + 1;
//	public static final int kHUDEye = kUserTiles + 2;
//	public static final int kHUDLeft2 = kUserTiles + 3;
//	public static final int kHUDRight2 = kUserTiles + 4;
//	public static final int kWideLoading = 9223;
//	public static final int kUltraWideLoading = 9222;

//	public static final int[][] kTileReplace = {
//		{ 2574, 9221 }
//	};
	
	public static int kSurfNone = 0;
	public static int kSurfStone = 1;
	public static int kSurfMetal = 2;
	public static int kSurfWood = 3;
	public static int kSurfFlesh = 4;
	public static int kSurfWater = 5;
	public static int kSurfDirt = 6;
	public static int kSurfClay = 7;
	public static int kSurfSnow = 8;
	public static int kSurfIce = 9;
	public static int kSurfLeaves = 10;
	public static int kSurfCloth = 11;
	public static int kSurfPlant = 12;
	public static int kSurfGoo = 13;
	public static int kSurfLava = 14;
	public static int kSurfMax = 15;
	
	public static int[] surfSfxLand = {
		-1,
		600,
		601, 
		602,
		603,
		604,
		605,
		605,
		605,
		600,
		605,
		605,
		605,
		604,
		603,
	};

	public static int[][] surfSfxMove = {
		new int[] {-1,-1},
		new int[] {802,803},
		new int[] {804,805},
		new int[] {806,807},
		new int[] {808,809},
		new int[] {810,811},
		new int[] {812,813},
		new int[] {814,815},
		new int[] {816,817},
		new int[] {818,819},
		new int[] {820,821},
		new int[] {822,823},
		new int[] {824,825},
		new int[] {826,827},
		new int[] {828,829},
	};
	
	public static short[] gVoxelData = new short[kMaxTiles];
	public static byte[] surfType = new byte[kMaxTiles];
	public static byte[] shadeTable = new byte[kMaxTiles];
	
	public static void tileInit()
	{
		aPicInit();
		voxelsInit("VOXEL.DAT");
		surfaceInit("SURFACE.DAT");
		shadeInit("SHADE.DAT");
	}
	
	public static void voxelsInit(String name) {
		Arrays.fill(gVoxelData, (byte) -1);
		
		ResourceData bb;
		if ((bb = BuildGdx.cache.getData(name, 0)) != null)
		{
	    	for(int i = 0; i < bb.capacity() / 2; i++) 
	    		gVoxelData[i] = bb.getShort();
		}
	}

	public static void tileLoadVoxel(int nTile)
	{
		if(nTile < 0 || (gPicAnm[nTile].view != 6 && gPicAnm[nTile].view != 7)) return;
		int nVoxel = gVoxelData[nTile];

		if(game.currentDef.mdInfo.getVoxel(nTile) == null && nVoxel >= 0 && nVoxel < 512)
		{
			ResourceData buffer = BuildGdx.cache.getData(nVoxel, "KVX");
			if(buffer != null) {
				Voxel vox = KVXLoader.load(buffer);
//				vox.loadskin(0, false); //XXX isUseShader
				game.currentDef.mdInfo.addVoxelInfo(vox, nTile);
			}
		}
	}
	
	public static void aPicInit() {
		for(int i = 0; i < kMaxTiles; i++)
			gPicAnm[i] = new PICANM(picanm[i]);
	}
	
	public static void surfaceInit(String name)
	{
		ResourceData data = BuildGdx.cache.getData(name, 0);
		if(data == null) return;
		
		int pos = 0;
		while(data.hasRemaining())
		{
			data.get(surfType, pos, Math.min(surfType.length, data.remaining()));
			pos += surfType.length;
		}
	}
	
	public static void shadeInit(String name)
	{
		ResourceData data = BuildGdx.cache.getData(name, 0);
		if(data == null) return;
		
		int pos = 0;
		while(data.hasRemaining())
		{
			data.get(shadeTable, pos, Math.min(shadeTable.length, data.remaining()));
			pos += shadeTable.length;
		}
	}
	
	/*******************************************************************************
	FUNCTION:		tileGetSurfType()

	DESCRIPTION:	Helper function to get the surface type for a game object.

	PARAMETERS:		nHit is the type and index of the hit object.

	RETURNS:		The surface type of the hit object.

	NOTES:
	*******************************************************************************/
	public static byte tileGetSurfType( int nHit )
	{
		int nHitType = nHit & kHitTypeMask;
		int nHitIndex = nHit & kHitIndexMask;
		switch ( nHitType )
		{
			case kHitFloor:
				return surfType[sector[nHitIndex].floorpicnum];

			case kHitCeiling:
				return surfType[sector[nHitIndex].ceilingpicnum];

			case kHitWall:
				return surfType[wall[nHitIndex].picnum];

			case kHitSprite:
				return surfType[sprite[nHitIndex].picnum];
		}
		return 0;
	}

	public static void tilePreloadTile( int nTile ) {
		if(nTile < 0 || nTile >= kMaxTiles)
			return;
		
		int view = 0;
		switch( gPicAnm[nTile].view ) {
		case 0: view = 1; break;
		case 1: view = 5; break;
		case 2: view = 8; break;
		case 3: view = 2; break;
		case 6:
		case 7: 
			tileLoadVoxel(nTile);
		    break;
		default:
		    break;
		}

		while ( view > 0 )
		{
			if ( gPicAnm[nTile].type != 0 )
			{
		    	for ( int i = gPicAnm[nTile].frames; i >= 0; i-- )
		    	{
			        if ( gPicAnm[nTile].type == 3 )
			        	gPrecacheScreen.addTile(nTile - i);
			        else gPrecacheScreen.addTile(nTile + i);
		    	}
		    } else gPrecacheScreen.addTile(nTile);

			nTile += gPicAnm[nTile].frames + 1;
		    view--;
		}
	}
	
	public static void loadGdxDef(DefScript baseDef)
	{
		FileHandle fil = Gdx.files.internal("bloodgdx.dat");
		if(fil != null)
		{
			DataResource res = new DataResource(null, fil.name(), -1, fil.readBytes());
			Group group = BuildGdx.cache.add(res, fil.name());
			
			GroupResource def = group.open(appdef);
			if(def != null)
			{
	    		baseDef.loadScript(fil.name(), def.getBytes());
	    		def.close();
			}
		}
	}

//	public static void tileLoadUserRes()
//	{
////		InputStream input = Main.class.getResourceAsStream("/BloodGDX.ART");
////		byte[] buffer = null;
////		if(input != null)
////		{
////			try {
////				buffer = new byte[input.available()];
////				input.read(buffer);
////			} catch (IOException e) {}
////			ByteBuffer bb = ByteBuffer.wrap(buffer);
////    		bb.order( ByteOrder.LITTLE_ENDIAN);
//		
//		FileHandle fil = Gdx.files.internal("BloodGDX.ART");
//		if(fil != null)
//		{
//			ByteBuffer bb = ByteBuffer.wrap(fil.readBytes());
//	    	bb.order( ByteOrder.LITTLE_ENDIAN);
//
//			int artversion = bb.getInt();
//			if (artversion != 1)
//				return;
//			
//			numtiles = bb.getInt();
//			int localtilestart = bb.getInt();
//			int localtileend = bb.getInt();
//			if(localtilestart >= MAXTILES || localtileend >= MAXTILES)
//				return;
//			
//			for (int i = localtilestart; i <= localtileend; i++) 
//				tilesizx[i] = bb.getShort();
//			for (int i = localtilestart; i <= localtileend; i++) 
//				tilesizy[i] = bb.getShort();
//			for (int i = localtilestart; i <= localtileend; i++)
//				picanm[i] = bb.getInt();
//			
//			for (int tilenume = localtilestart; tilenume <= localtileend; tilenume++) {
//				if(bb.position() == bb.capacity())
//					break;
//				int dasiz = tilesizx[tilenume] * tilesizy[tilenume];
//				waloff[tilenume] = new byte[dasiz];
//				bb.get(waloff[tilenume]);
//			}
//			bb.clear();
//			bb = null;
//		}
//		
//		for(int i = 0; i < kTileReplace.length; i++)
//		{
//			waloff[kTileReplace[i][0]] = waloff[kTileReplace[i][1]];
//			tilesizx[kTileReplace[i][0]] = tilesizx[kTileReplace[i][1]];
//			tilesizy[kTileReplace[i][0]] = tilesizy[kTileReplace[i][1]];
//		}
//	}
}
