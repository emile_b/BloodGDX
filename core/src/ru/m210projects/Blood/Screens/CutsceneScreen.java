// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import static ru.m210projects.BuildSmacker.Smacker.*;
import static ru.m210projects.Blood.Globals.kAngleMask;
import static ru.m210projects.Blood.Globals.kPalNormal;
import static ru.m210projects.Blood.Main.cfg;
import static ru.m210projects.Blood.SOUND.sndStopAllSounds;
import static ru.m210projects.Blood.Screen.*;
import static ru.m210projects.Blood.Strings.cutskip;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Build.Engine.MAXPALOOKUPS;
import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Engine.RESERVEDPALS;
import static ru.m210projects.Build.Engine.palookup;
import static ru.m210projects.Build.Engine.tilesizx;
import static ru.m210projects.Build.Engine.tilesizy;
import static ru.m210projects.Build.Engine.totalclock;
import static ru.m210projects.Build.Engine.waloff;
import static ru.m210projects.Build.Engine.xdim;
import static ru.m210projects.Build.Engine.ydim;
import static ru.m210projects.Build.Input.Keymap.ANYKEY;
import static ru.m210projects.Build.OnSceenDisplay.Console.OSDTEXT_RED;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.mulscale;

import com.badlogic.gdx.Gdx;

import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.Audio.Source;
import ru.m210projects.Build.FileHandle.Resource.ResourceData;
import ru.m210projects.Build.Audio.BuildAudio.Driver;
import ru.m210projects.Build.Loader.WAVLoader;
import ru.m210projects.Build.OnSceenDisplay.Console;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.BuildFont.TextAlign;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Build.Settings.BuildSettings;
import ru.m210projects.BuildSmacker.Smk;

public class CutsceneScreen extends SkippableAdapter {

	private Runnable callback;
	private int gCutsClock;

	private Smk smkfil;
	private long smktime;
	private long LastMS;
	private Source smkSource;
	private byte[] opalookup;

	private final int kSMKTile = MAXTILES - 3;

	public CutsceneScreen(BuildGame game) {
		super(game);
		opalookup = new byte[palookup[0].length];
		System.arraycopy(palookup[0], 0, opalookup, 0, opalookup.length);
	}

	@Override
	public void show() {
		sndStopAllSounds();
		
		if (smkSource != null) {
			smkSource.setGlobal(1);
			smkSource.play(1.0f);
		}
		
		if(game.pMenu.gShowMenu)
			game.pMenu.mClose();
		
		engine.sampletimer();
		LastMS = engine.getticks();
		gCutsClock = totalclock = 0;
	}
	
	@Override
	public void hide () {
		
		curPalette = -1;
		scrSetPalette(kPalNormal);
		scrReset();
		scrGLSetDac(0);
	}
	
	@Override
	public void skip() {
		smkClose();
		super.skip();
	}

	public CutsceneScreen setCallback(Runnable callback) {
		this.callback = callback;
		this.setSkipping(callback);
		return this;
	}

	public boolean init(String path, String sndPath) {
		if (!cfg.showCutscenes || smkfil != null)
			return false;

		ResourceData smkbuf = BuildGdx.cache.getData(path, 0);
		if (smkbuf == null)
			return false;

		if ((smkfil = smk_open_memory(smkbuf.getBuffer())) == null)
			return false;

		// Smacker frames are decoded linewise, but BUILD expects its
		// tiles columnwise, so we will treat the tile as though it's
		// rotated 90 degrees and flipped horizontally.

		smk_enable_all(smkfil, SMK_VIDEO_TRACK);
		smk_info_video(smkfil, true, true, false);
		int xsiz = info_w;
		int ysiz = info_h;

		tilesizx[kSMKTile] = (short) ysiz;
		tilesizy[kSMKTile] = (short) xsiz;
		waloff[kSMKTile] = null;

		for (int i = 0; i < MAXPALOOKUPS; i++)
			palookup[0][i] = (byte) i;
		
		changepalette();

		smkStartWAV(sndPath);
		smktime = 0;
		LastMS = -1;

		return true;
	}

	@Override
	public void draw(float delta) {
		if(!cfg.showCutscenes || !smkPlay() && skipCallback != null) {
			smkClose();
			if (callback != null) {
				Gdx.app.postRunnable(callback);
				callback = null;
			}
		}
		
		if (game.pInput.ctrlKeyStatus(ANYKEY)) 
			gCutsClock = totalclock;
		
		int shade = 32 + mulscale(32, Sin((20 * totalclock) & kAngleMask), 30);
		if (totalclock - gCutsClock < 200 && escSkip) // 2 sec 
			game.getFont(4).drawText(160, 5, cutskip, shade, MAXPALOOKUPS - RESERVEDPALS - 1, TextAlign.Center, 2, false);
	}
	
	private void changepalette()
	{
		byte[] pal = smkfil.video.palette;
		if(pal == null) return;
		
		engine.setbrightness(BuildSettings.paletteGamma.get(), smkfil.video.palette, 2);
	    
	    int white = -1;
	    int k = 0;
        for (int i = 0; i < 256; i+=3)
        {
            int j = (pal[3*i]&0xFF)+(pal[3*i+1]&0xFF)+(pal[3*i+2]&0xFF);
            if (j > k) { k = j; white = i; }
        }

        if(white == -1) return;
	    
        int palnum = MAXPALOOKUPS - RESERVEDPALS - 1;
	    byte[] remapbuf = new byte[768];
		for(int i = 0; i < 768; i++)
			remapbuf[i] = (byte) white;	
		engine.makepalookup(palnum, remapbuf,0, 1, 0, 1);
		
		for(int i = 0; i < 256; i++) {
			int tile = game.getFont(4).getTile(i);
			if(tile >= 0) 
				engine.invalidatetile(tile, palnum, -1);
		}
	}

	private boolean smkPlay() {
		if (smkfil != null) {
			long ms = engine.getticks();
			long dt = ms - LastMS;
			smktime += dt;

			float tick = (float) (smkfil.pts_inc / 1000f);
			if (smktime >= tick) {
				if (smkfil.cur_frame < smkfil.frames) {
					if (smk_render_frame(smkfil, smkfil.cur_frame) != 0) 
						changepalette();
					
					waloff[kSMKTile] = smk_get_video(smkfil);
					engine.invalidatetile(kSMKTile, 0, -1);
					smkfil.cur_frame++;
				} else
					return false;

				smktime -= tick;
			}
			LastMS = ms;
			if (tilesizx[kSMKTile] <= 0)
				return false;

			if (waloff[kSMKTile] != null)
				engine.rotatesprite(160 << 16, 100 << 16, (int) divscale(200, tilesizx[kSMKTile], 16), 512, kSMKTile, 0,
						0, 2 | 4 | 8 | 64, 0, 0, xdim - 1, ydim - 1);

			return true;
		}

		return false;
	}

	private void smkStartWAV(String sampleName) {
		if (!BuildGdx.audio.IsInited(Driver.Sound))
			return;

		if (cfg.noSound || sampleName == null || sampleName.length() == 0)
			return;

		byte[] buf = BuildGdx.cache.getBytes(sampleName, 0);
		if (buf == null) {
			Console.Println("Could not load wav file: " + sampleName, OSDTEXT_RED);
			return;
		}

		int sampleSize = buf.length;
		if (sampleSize <= 0)
			return;

		try {
			WAVLoader wav = new WAVLoader(buf);
			BuildGdx.audio.getSound().resetListener();
			smkSource = BuildGdx.audio.newSound(wav.sampledata, wav.samplerate, wav.samplebits, wav.channels, 255);
		} catch (Exception e) {
			Console.Println(e.getMessage() + " in " + sampleName, OSDTEXT_RED);
			return;
		}
	}
	

	private void smkClose() {
		if (smkfil != null)
			System.arraycopy(opalookup, 0, palookup[0], 0, opalookup.length);

		smkfil = null;
		if (smkSource != null)
			smkSource.dispose();
		smkSource = null;
		LastMS = -1;
		
		curPalette = -1;
		scrSetPalette(kPalNormal);
		scrReset();
	}
}
