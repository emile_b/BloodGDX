// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import ru.m210projects.Build.BitHandler;

public class PICANM {
	public int frames;	// number of frames - 1
	public int update;	// this came from upper bit of frames
	public int type;		// 0 = none, 1 = Oscil, 2 = Frwd, 3 = Bkwd
		
	public byte xcenter;	
	public byte ycenter;
		
	public int speed;	// (clock >> speed) determines rate
	public int view;
//	public int registered;
	
	public PICANM(int picanm) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order( ByteOrder.LITTLE_ENDIAN);
		byte[] data = bb.putInt(picanm).array();
		
		frames = BitHandler.buread(data, 0, 0, 4);
		update = BitHandler.buread(data, 0, 5, 5);
		type = BitHandler.buread(data, 0, 6, 7);
		
		xcenter = (byte) BitHandler.bsread(data, 1, 0, 7);
		ycenter = (byte) BitHandler.bsread(data, 2, 0, 7);
		
		speed = BitHandler.buread(data, 3, 0, 3);
		view = BitHandler.buread(data, 3, 4, 6);
		
		bb.clear();
		bb = null;
		data = null;

		/*
		frames =  picanm & 0x0000001F;
		update =  picanm & 0x00000020;	
		type =    picanm & 0x000000C0;
		
		xcenter = (byte) ((picanm & 0x0000FF00) >> 8);
		ycenter = (byte) ((picanm & 0x00FF0000) >> 16);
		
		speed = (picanm & 0x0F000000) >> 27; // ida: (16 * picanm) >> 28
		view = (int) ((2L * (picanm & 0xFFFFFFFFL)) >> 29);
		*/
	}
}
