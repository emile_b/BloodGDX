// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;

import static ru.m210projects.Build.OnSceenDisplay.Console.OSDTEXT_GOLD;
import static ru.m210projects.Build.FileHandle.Compat.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.FileHandle.FileEntry;
import ru.m210projects.Build.FileHandle.FileUtils;
import ru.m210projects.Build.FileHandle.Resource;
import ru.m210projects.Build.OnSceenDisplay.Console;

public class DEMO {
	
	public static List<String> demofiles = new ArrayList<String>();
	public static final String kBloodDemSig	=	"DEM\032";
	
	public int 	   nInputCount;			   // number of INPUT structures recorded
	public int     nNetPlayers;            // number of players recorded
	public short   nMyConnectIndex;        // the ID of the player
	public short   nConnectHead;           // index into connectPoints of Player
	public short[] connectPoints = new short[kMaxPlayers];      // IDs of players
	
	public String signature;
	public int nVersion;
	public int rcnt;
	
	public INPUT pDemoInput[][]; // = new INPUT[16384][kMaxPlayers];
	
	public DEMO(ByteBuffer bb) {
		byte[] buf = new byte[4];
		bb.get(buf);
		signature = new String(buf);
		nVersion = bb.getShort();
		
		if(nVersion == 277)
			bb.getInt(); //int nBuild - for demos > 1.10
		
		/*
		   // build of Blood that created the demo
           // builds are:
           // SW, SWCD:   2
           // Registered: 3
           // Plasma Pak: 4
		*/

		nInputCount = bb.getInt();
		nNetPlayers = bb.getInt();
		nMyConnectIndex = bb.getShort();
		nConnectHead = bb.getShort();
		for(int i = 0; i < 8; i++)
			connectPoints[i] = bb.getShort();
		
		//GameInfo
		pGameInfo.nGameType = bb.get() & 0xFF;
		pGameInfo.nDifficulty = bb.get() & 0xFF;
		pGameInfo.nEpisode = bb.getInt();       
		pGameInfo.nLevel = bb.getInt();    
		
		buf = new byte[144];
		bb.get(buf);
		String name = new String(buf);
		int index = -1;
		if((index = name.indexOf(0)) != -1) 
			name = name.substring(0, index).toLowerCase();
		if(FileUtils.isExtension(name, "map"))
			name = name.substring(0, name.lastIndexOf('.'));
		
		pGameInfo.zLevelName = name;
		bb.get(buf);
		pGameInfo.zLevelSong = new String(buf);
		if((index = pGameInfo.zLevelSong.indexOf(0)) != -1) 
			pGameInfo.zLevelSong = pGameInfo.zLevelSong.substring(0, index);
		pGameInfo.nTrackNumber = bb.getInt();
        for(int i = 0; i < 4; i++)
        	bb.getInt(); //szSaveGameName
        for(int i = 0; i < 4; i++)
        	bb.getInt(); //szUserGameName
        bb.getShort(); //nSaveGameSlot       
        bb.getInt(); //picEntry 
        pGameInfo.uMapCRC = bb.getInt(); //uMapCRC

        pGameInfo.nMonsterSettings = bb.get() & 0xFF;             
        pGameInfo.uGameFlags = bb.getInt();                
        pGameInfo.uNetGameFlags = bb.getInt();      
        pGameInfo.nWeaponSettings = bb.get() & 0xFF;        
        pGameInfo.nItemSettings   = bb.get() & 0xFF;        
        pGameInfo.nRespawnSettings = bb.get() & 0xFF;     
        pGameInfo.nTeamSettings = bb.get() & 0xFF;        
        pGameInfo.nMonsterRespawnTime = bb.getInt();               
        pGameInfo.nWeaponRespawnTime  = bb.getInt();                   
        pGameInfo.nItemRespawnTime  = bb.getInt();                     
        pGameInfo.nSpecialRespawnTime  = bb.getInt();   
        
        pGameInfo.nEnemyQuantity = pGameInfo.nDifficulty;
        pGameInfo.nEnemyDamage = pGameInfo.nDifficulty;
        pGameInfo.nPitchforkOnly = false;
        
        if(pDemoInput == null || pDemoInput.length <= nInputCount)
        	pDemoInput = new INPUT[nInputCount][kMaxPlayers];

		buf = new byte[22];
		for(int rcnt = 0; rcnt < nInputCount; rcnt++)
			for ( int i = nConnectHead; i >= 0; i = connectPoints[i] )
		    {
				if((bb.position() + buf.length) > bb.capacity()) return;
				bb.get(buf);
				pDemoInput[rcnt][i] = new INPUT(buf, nVersion);
		    }
		
		rcnt = 0;
	}
	
	public static int nDemonum = -1;
	public static DEMO demfile = null;
	public static final char[] demfilename = { 'B', 'L', 'O', 'O', 'D', 'X',
			'X', 'X', '.', 'D', 'E', 'M' };

	public static void demoscan() {
		byte[] buf = new byte[4];

		Resource fil = null;
		for (Iterator<FileEntry> it = BuildGdx.compat.getDirectory(Path.Game).getFiles().values().iterator(); it.hasNext();) {
			FileEntry file = it.next();
			if (file.getExtension().equals("dem")) {
				String name = file.getFile().getName();
				if ((fil = BuildGdx.compat.open(file)) != null) {
					fil.read(buf, 4);
					String signature = new String(buf);
					int version = fil.readShort();
					if (signature.equals(DEMO.kBloodDemSig) && (version == 277)) 											
					demofiles.add(name);
					fil.close();
				}
			}
		}
		
		if(demofiles.size() != 0)
			Collections.sort(demofiles);
		Console.Println("There are " + demofiles.size() + " demo(s) in the loop", OSDTEXT_GOLD);
		
		if(cfg.gDemoSeq == 2)
		{
			int nextnum = nDemonum;
			if(demofiles.size() > 1) {
				while(nextnum == nDemonum) 
					nextnum = (int) (Math.random() * (demofiles.size()));
			}
			nDemonum = nextnum;
		}
	}
	
	public static boolean IsOriginalDemo() {
		if((pGameInfo.nGameType == kNetModeOff && cfg.gVanilla) || (game.isCurrentScreen(gDemoScreen) && demfile != null && demfile.nVersion == 277))
			return true;

		return false;
	}
	
//	public static void DemoReset() {
//		engine.compatibleMode = false;
//		nDemonum = 0;
//		demfile = null;
//	}
}
