// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import static ru.m210projects.Blood.EVENT.getIndex;
import static ru.m210projects.Blood.EVENT.getType;
import static ru.m210projects.Blood.EVENT.kMaxChannels;
import static ru.m210projects.Blood.EVENT.kMaxID;
import static ru.m210projects.Blood.LOADSAVE.gdxSave;
import static ru.m210projects.Blood.LOADSAVE.SAVEINFO;
import static ru.m210projects.Blood.LOADSAVE.SAVESCREENSHOTSIZE;
import static ru.m210projects.Blood.LOADSAVE.SAVEGDXDATA;
import static ru.m210projects.Blood.Mirror.MAXMIRRORS;
import static ru.m210projects.Blood.Trigger.kMaxBusyArray;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.kMaxSequences;
import static ru.m210projects.Blood.DB.kMaxXSectors;
import static ru.m210projects.Blood.DB.kMaxXSprites;
import static ru.m210projects.Blood.DB.kMaxXWalls;
import static ru.m210projects.Blood.Globals.SS_CEILING;
import static ru.m210projects.Blood.Globals.SS_FLOOR;
import static ru.m210projects.Blood.Globals.SS_MASKED;
import static ru.m210projects.Blood.Globals.SS_SPRITE;
import static ru.m210projects.Blood.Globals.SS_WALL;
import static ru.m210projects.Blood.Globals.kMaxPlayers;
import static ru.m210projects.Blood.Globals.kMaxSectors;
import static ru.m210projects.Blood.Globals.kMaxSprites;
import static ru.m210projects.Blood.Globals.kMaxStatus;
import static ru.m210projects.Blood.Globals.kMaxWalls;
import static ru.m210projects.Blood.Globals.kStatFree;
import static ru.m210projects.Blood.LEVELS.kMaxMap;
import static ru.m210projects.Blood.LEVELS.levelGetEpisode;
import static ru.m210projects.Build.Engine.MAXPSKYTILES;
import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Engine.MAXWALLSV7;

import java.io.FileNotFoundException;
import java.util.Arrays;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.Seq.CeilingInst;
import ru.m210projects.Blood.Types.Seq.FloorInst;
import ru.m210projects.Blood.Types.Seq.MaskedWallInst;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Blood.Types.Seq.SpriteInst;
import ru.m210projects.Blood.Types.Seq.WallInst;
import ru.m210projects.Build.BitHandler;
import ru.m210projects.Build.Strhandler;
import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.FileHandle.FileUtils;
import ru.m210projects.Build.FileHandle.Resource.ResourceData;
import ru.m210projects.Build.Types.Hitscan;
import ru.m210projects.Build.Types.SECTOR;
import ru.m210projects.Build.Types.SPRITE;
import ru.m210projects.Build.Types.WALL;

public class SafeLoader {

	public short pskyoff[] = new short[MAXPSKYTILES], pskybits;
	public int parallaxyscale;
	
	public short connecthead, connectpoint2[] = new short[kMaxPlayers];
	public int randomseed;
	
	public int visibility, parallaxvisibility;
	public byte automapping;
	public byte[] show2dsector = new byte[(kMaxSectors + 7) >> 3];
	public byte[] show2dwall = new byte[(kMaxWalls + 7) >> 3];
	public byte[] show2dsprite = new byte[(kMaxSprites + 7) >> 3];
	
	public byte[] gotpic = new byte[(MAXTILES + 7) >> 3];
	public byte[] gotsector = new byte[(kMaxSectors + 7) >> 3];
	
	public Hitscan safeHitInfo;
	
	//MapInfo
	
	public short numsectors, numwalls, numsprites;

	public short[] headspritesect, headspritestat;
	public short[] prevspritesect, prevspritestat;
	public short[] nextspritesect, nextspritestat;

	public SECTOR[] sector = new SECTOR[kMaxSectors];
	public WALL[] wall = new WALL[kMaxWalls];
	public SPRITE[] sprite = new SPRITE[kMaxSprites];

	//Blood variables
	
	public Vector2 kwall[] = new Vector2[MAXWALLSV7];
	public Vector3 ksprite[] = new Vector3[kMaxSprites];
	
	public int secFloorZ[] = new int[kMaxSectors];
	public int secCeilZ[] = new int[kMaxSectors];
	public int secPath[] = new int[kMaxSectors];
	
	public long sprXVel[] = new long[kMaxSprites];
	public long sprYVel[] = new long[kMaxSprites];
	public long sprZVel[] = new long[kMaxSprites];

	public long floorVel[] = new long[kMaxSectors];
	public long ceilingVel[] = new long[kMaxSectors];
	
	public short nStatSize[] = new short[kMaxStatus+1];
	
	public XSPRITE[] xsprite = new XSPRITE[kMaxXSprites];
	public XWALL[] xwall = new XWALL[kMaxXWalls];
	public XSECTOR[] xsector = new XSECTOR[kMaxXSectors];

	public int nextXSprite[] = new int[kMaxXSprites];
	public int nextXWall[] = new int[kMaxXWalls];
	public int nextXSector[] = new int[kMaxXSectors];
	
	public GAMEINFO safeGameInfo = new GAMEINFO();
	
	public boolean showinvisibility;
	public boolean gNoClip, gFogMode, gFullMap, gPaused, gInfiniteAmmo, cheatsOn;
	public int gSkyCount;
	public int gFrameClock, gTicks, gFrame, gGameClock;

	public int[] cumulDamage = new int[kMaxXSprites];
	public int[] gDudeSlope = new int[kMaxXSprites];
	
	public ZONE gStartZone[] = new ZONE[kMaxPlayers];
	public int[] gUpperLink = new int[kMaxSectors], gLowerLink = new int[kMaxSectors];
	
	public int mirrorcnt;
	public int[] MirrorType = new int[MAXMIRRORS];
	public int[] MirrorX = new int[MAXMIRRORS];
	public int[] MirrorY = new int[MAXMIRRORS];
	public int[] MirrorZ = new int[MAXMIRRORS];
	public int[] MirrorLower = new int[MAXMIRRORS];
	public int[] MirrorUpper = new int[MAXMIRRORS];
	public int MirrorSector;
	public int[] MirrorWall = new int[4];
	
	public SeqInst siWall[] = new SeqInst[kMaxXWalls], siMasked[] = new SeqInst[kMaxXWalls];
	public SeqInst siCeiling[] = new SeqInst[kMaxXSectors], siFloor[] = new SeqInst[kMaxXSectors];
	public SeqInst siSprite[] = new SeqInst[kMaxXSprites];
	
	public short actListIndex[] = new short[kMaxSequences];
	public byte actListType[] = new byte[kMaxSequences];
	
	public int activeCount;
	
	public int rxBucketIndex[] = new int[kMaxChannels];
	public int rxBucketType[] = new int[kMaxChannels];
	public short bucketHead[] = new short[kMaxID + 1];
	
	public int qEventEvent[] = new int[1025];
	public int qEventPriority[] = new int[1025];
	public int fNodeCount;
	
	public int gBusyCount = 0;
	public BUSY gBusy[] = new BUSY[kMaxBusyArray];
	
	public int[] nTeamCount = new int[kMaxPlayers];
	public int gNetPlayers;
	
	public byte[] autoaim = new byte[kMaxPlayers];
	public byte[] slopetilt = new byte[kMaxPlayers];
	public byte[] skill = new byte[kMaxPlayers];
	public String[] name = new String[kMaxPlayers];

	public PLAYER safePlayer[] = new PLAYER[kMaxPlayers];
	
	public SPRITEHIT gSpriteHit[] = new SPRITEHIT[kMaxXSprites];
	public int[] gWallExp = new int[kMaxWalls];
	public int[] gSectorExp = new int[kMaxSectors];
	public byte[] gSpriteExp = new byte[(kMaxSectors + 7) >> 3];
	
	public POSTPONE[] gPost = new POSTPONE[kMaxSprites];
	public int gPostCount;
	
	public int gNextMap;
	public int foundSecret;
	public int totalSecrets;
	public int superSecrets;
	public int totalKills;
	public int kills;
	
	public int deliriumTilt = 0;
	public int deliriumTurn = 0;
	public int deliriumPitch = 0;
	
	public boolean gUserEpisode, gForceMap;
	public BloodIniFile addon;
	public String addonFileName;
	private String message;
	
	public SafeLoader()
	{
		headspritesect = new short[MAXSECTORS + 1]; 
		headspritestat = new short[MAXSTATUS + 1];
		prevspritesect = new short[MAXSPRITES]; 
		prevspritestat = new short[MAXSPRITES];
		nextspritesect = new short[MAXSPRITES]; 
		nextspritestat = new short[MAXSPRITES];

		for(int i = 0; i < MAXSPRITES; i++) 
			sprite[i] = new SPRITE();
		for(int i = 0; i < MAXSECTORS; i++)
			sector[i] = new SECTOR();
		for(int i = 0; i < kMaxWalls; i++)
			wall[i] = new WALL();
		
		for(int i = 0; i < MAXWALLSV7; i++)
			kwall[i] = new Vector2();
		for(int i = 0; i < kMaxSprites; i++) {
			ksprite[i] = new Vector3();
			gPost[i] = new POSTPONE();
		}
		
		for(int i = 0; i < kMaxXSprites; i++) {
			xsprite[i] = new XSPRITE();
			gSpriteHit[i] = new SPRITEHIT();
		}
		for(int i = 0; i < kMaxXWalls; i++)
			xwall[i] = new XWALL();
		for(int i = 0; i < kMaxXSectors; i++)
			xsector[i] = new XSECTOR();
		
		for(int i = 0; i < kMaxPlayers; i++) {
			gStartZone[i] = new ZONE();
			safePlayer[i] = new PLAYER();
		}
		
		for(int i = 0; i < kMaxXWalls; i++)
			siWall[i] = new WallInst();
		for(int i = 0; i < kMaxXWalls; i++)
			siMasked[i] = new MaskedWallInst();
		for(int i = 0; i < kMaxXSectors; i++)
			siCeiling[i] = new CeilingInst();
		for(int i = 0; i < kMaxXSectors; i++)
			siFloor[i] = new FloorInst();
		for(int i = 0; i < kMaxXSprites; i++)
			siSprite[i] = new SpriteInst();
		
		for(int i = 0; i < kMaxBusyArray; i++)
			gBusy[i] = new BUSY();
		
		safeHitInfo = new Hitscan();
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public boolean load(ResourceData bb, int nVersion)
	{
		addon = null;
		addonFileName = null;
		message = null;
		gUserEpisode = false;
		try {
			for(int i = 0; i < MAXWALLSV7; i++) 
				kwall[i].set(0, 0);
			for(int i = 0; i < kMaxSprites; i++) 
				ksprite[i].set(0, 0, 0);
			Arrays.fill(gotpic, (byte) 0);
			
			for (int i = 0; i < kMaxPlayers; i++) {
				autoaim[i] = -1;
				slopetilt[i] = -1;
				skill[i] = -1;
				name[i] = null;
			}
			
			if(nVersion == 0x100) { //old save file format
				bb.rewind();
				MyLoad100(bb);
				DudesLoad(bb);
				WarpLoad(bb);
				MirrorLoad(bb);
				SeqLoad(bb, nVersion);
				EventLoad(bb);
				TriggersLoad(bb);
				PlayersLoad(bb, nVersion);
				ActorsLoad(bb);
				bb.getShort(); //unknown
				GameInfoLoad(bb);
				StatsLoad(bb);
				ScreenLoad(bb);
			}
			else
			{
				if(nVersion >= gdxSave)
				{
					bb.position(SAVEINFO + SAVESCREENSHOTSIZE); //Save header + screenshot
					LoadGDXBlock(bb);
				}  
				
				MyLoad110(nVersion, bb);
				DudesLoad(bb);
				cheatsOn = bb.get() == 1;
				WarpLoad(bb); 
				MirrorLoad(bb);
				SeqLoad(bb, nVersion);
				EventLoad(bb);
				TriggersLoad(bb);
				PlayersLoad(bb, 277);
				ActorsLoad(bb);
				GameInfoLoad(bb);
				StatsLoad(bb);
				ScreenLoad(bb); 
			}
			
			if(gUserEpisode) { //try to find addon
				addon = levelGetEpisode(addonFileName);
				if(addon == null)
				{
					message = "Can't find user episode file: " + addonFileName;
					gUserEpisode = false;
					
					safeGameInfo.nEpisode = 0;
					safeGameInfo.nLevel = kMaxMap;
					gForceMap = true;
				}
			}
			
			if(bb.position() == bb.capacity())
				return true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void MyLoad100(ResourceData bb)
	{
		int kMaxTiles = 4096;
		
		LoadGameInfo(bb);

		gFrameClock = bb.getInt(); 
		gTicks = bb.getInt(); 
		gFrame = bb.getInt(); 
		gGameClock = bb.getInt(); 
		
		gPaused = (bb.get() & 0xFF) == 1;
		bb.get(); //unk_110D21

		for(int i = 0; i < MAXWALLSV7; i++) {
			kwall[i].x = bb.getInt();
			kwall[i].y = bb.getInt();
		}
		
		for(int i = 0; i < kMaxSprites; i++) {
			ksprite[i].x = bb.getInt();
			ksprite[i].y = bb.getInt();
			ksprite[i].z = bb.getInt();
		}

		for(int i = 0; i < kMaxSectors; i++) 
			secFloorZ[i] = bb.getInt();
		for(int i = 0; i < kMaxSectors; i++) 
			secCeilZ[i] = bb.getInt();
		for(int i = 0; i < kMaxSectors; i++) 
			floorVel[i] = bb.getInt();
		for(int i = 0; i < kMaxSectors; i++) 
			ceilingVel[i] = bb.getInt();

		safeHitInfo.hitsect =  bb.getShort();
		safeHitInfo.hitwall =  bb.getShort();
		safeHitInfo.hitsprite = bb.getShort();
		safeHitInfo.hitx =  bb.getInt(); 
		safeHitInfo.hity =  bb.getInt(); 
		safeHitInfo.hitz =  bb.getInt();

		gForceMap = bb.get() == 1;
		
		for(int i = 0; i <= kMaxStatus; i++)
			nStatSize[i] = bb.getShort();
		
		byte[] buf = new byte[XSPRITE.sizeof];
		for(int i = 0; i < kMaxXSprites; i++) {
			bb.get(buf);
			xsprite[i].init(buf);
			xsprite[i].reference = BitHandler.bsread(buf, 0, 0, 13);
		}

		buf = new byte[XWALL.sizeof];
		for(int i = 0; i < kMaxXWalls; i++) {
			bb.get(buf);
			xwall[i].init(buf);
			xwall[i].reference = BitHandler.bsread(buf, 0, 0, 13);
		}	
		buf = new byte[XSECTOR.sizeof];
		for(int i = 0; i < kMaxXSectors; i++) {
			bb.get(buf);
			xsector[i].init(buf);
			xsector[i].reference = BitHandler.bsread(buf, 0, 0, 13);
		}
		
		for(int i = 0; i < kMaxSprites; i++) 
			sprXVel[i] = bb.getInt();
		for(int i = 0; i < kMaxSprites; i++) 
			sprYVel[i] = bb.getInt();
		for(int i = 0; i < kMaxSprites; i++) 
			sprZVel[i] = bb.getInt();
		for(int i = 0; i < kMaxXSprites; i++) 
			nextXSprite[i] = bb.getShort();
		for(int i = 0; i < kMaxXWalls; i++) 
			nextXWall[i] = bb.getShort();
		for(int i = 0; i < kMaxXSectors; i++) 
			nextXSector[i] = bb.getShort();
			
		bb.getInt(); //dword_176518
		bb.getInt(); //dword_17651C
		
		gSkyCount = bb.getInt();
		gFogMode = bb.get() == 1;

		for(int i = 0; i < kMaxSectors; i++) 
			sector[i].buildSector(bb);

		for(int i = 0; i < MAXWALLSV7; i++) 
			wall[i].buildWall(bb);
		
		for(int i = 0; i < kMaxSprites; i++) 
			sprite[i].buildSprite(bb);

		numsectors = bb.getShort();
		numwalls = bb.getShort();
		randomseed = bb.getInt();
		bb.get(); //parallaxtype
		
		showinvisibility = bb.get() == 1;
		bb.getInt(); //parallaxyoffs
		
		parallaxyscale = bb.getInt();
		visibility = bb.getInt();
		parallaxvisibility = bb.getInt();
		
		for(int i = 0; i < MAXPSKYTILES; i++)
			pskyoff[i] = bb.getShort();
		pskybits = bb.getShort();

		for(int i = 0; i <= kMaxSectors; i++)
			headspritesect[i] = bb.getShort();
		for(int i = 0; i <= kMaxStatus; i++)
			headspritestat[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			prevspritesect[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			prevspritestat[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			nextspritesect[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			nextspritestat[i] = bb.getShort();
		for(int i = 0; i < (kMaxSectors+7)>>3; i++)
			show2dsector[i] = bb.get();
		for(int i = 0; i < (MAXWALLSV7+7)>>3; i++)
			show2dwall[i] = bb.get();
		for(int i = 0; i < (kMaxSprites+7)>>3; i++)
			show2dsprite[i] = bb.get();
		automapping = bb.get();
		for(int i = 0; i < (kMaxTiles+7)>>3; i++)
			gotpic[i] = bb.get();
		for(int i = 0; i < (kMaxSectors+7)>>3; i++)
			gotsector[i] = bb.get();
		
		safeGameInfo.nEnemyDamage = safeGameInfo.nDifficulty;
		safeGameInfo.nEnemyQuantity = safeGameInfo.nDifficulty;
		safeGameInfo.nPitchforkOnly = false;
		gInfiniteAmmo = false;
		cheatsOn = false;
	}
	
	public void MyLoad110(int nVersion, ResourceData bb)
	{
		int kMaxTiles = 6144;
		
		LoadGameInfo(bb);
		numsectors = bb.getShort();
		numwalls = bb.getShort();
		numsprites = (short) bb.getInt();

		for(int i = 0; i < numsectors; i++) 
			sector[i].buildSector(bb);
		for(int i = 0; i < numwalls; i++) 
			wall[i].buildWall(bb);
		for(int i = 0; i < kMaxSprites; i++) 
			sprite[i].buildSprite(bb);
		randomseed = bb.getInt();
		bb.get(); //parallaxtype
		
		showinvisibility = bb.get() == 1;
		bb.getInt(); //parallaxyoffs
		
		parallaxyscale = bb.getInt();
		visibility = bb.getInt();
		parallaxvisibility = bb.getInt();
		
		for(int i = 0; i < MAXPSKYTILES; i++)
			pskyoff[i] = bb.getShort();
		pskybits = bb.getShort();

		for(int i = 0; i <= kMaxSectors; i++)
			headspritesect[i] = bb.getShort();
		for(int i = 0; i <= kMaxStatus; i++)
			headspritestat[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			prevspritesect[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			prevspritestat[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			nextspritesect[i] = bb.getShort();
		for(int i = 0; i < kMaxSprites; i++)
			nextspritestat[i] = bb.getShort();
		for(int i = 0; i < (kMaxSectors+7)>>3; i++)
			show2dsector[i] = bb.get();
		for(int i = 0; i < (MAXWALLSV7+7)>>3; i++)
			show2dwall[i] = bb.get();
		for(int i = 0; i < (kMaxSprites+7)>>3; i++)
			show2dsprite[i] = bb.get();
		automapping = bb.get();
		for(int i = 0; i < (kMaxTiles+7)>>3; i++)
			gotpic[i] = bb.get();
		for(int i = 0; i < (kMaxSectors+7)>>3; i++)
			gotsector[i] = bb.get();
		
		gFrameClock = bb.getInt();
		gTicks = bb.getInt(); 
		gFrame = bb.getInt(); 
		gGameClock = bb.getInt(); 
		
		gPaused = (bb.get() & 0xFF) == 1;
		bb.get(); //unk_133921

		for(int i = 0; i < numwalls; i++) {
			kwall[i].x = bb.getInt();
			kwall[i].y = bb.getInt();
		}
		
		for(int i = 0; i < numsprites; i++) {
			int x = bb.getInt();
			int y = bb.getInt();
			int z = bb.getInt();
			if(i >= kMaxSprites)
				continue;

			ksprite[i].x = x;
			ksprite[i].y = y;
			ksprite[i].z = z;
		}
		
		for(int i = 0; i < numsectors; i++) 
			secFloorZ[i] = bb.getInt();
		for(int i = 0; i < numsectors; i++) 
			secCeilZ[i] = bb.getInt();
		for(int i = 0; i < numsectors; i++) 
			floorVel[i] = bb.getInt();
		for(int i = 0; i < numsectors; i++) 
			ceilingVel[i] = bb.getInt();
		
		safeHitInfo.hitsect =  bb.getShort();
		safeHitInfo.hitwall =  bb.getShort();
		safeHitInfo.hitsprite = bb.getShort();
		safeHitInfo.hitx =  bb.getInt(); 
		safeHitInfo.hity =  bb.getInt(); 
		safeHitInfo.hitz =  bb.getInt();
		
		gForceMap = bb.get() == 1;
		bb.get(); //crypted
		bb.get(); //crypted matt

		byte[] buf = new byte[128];
		bb.get(buf, 0, 128); //size of xsystem and copyright

		for(int i = 0; i <= kMaxStatus; i++)
			nStatSize[i] = bb.getShort();
		for(int i = 0; i < kMaxXSprites; i++) 
			nextXSprite[i] = bb.getShort();
		for(int i = 0; i < kMaxXWalls; i++) 
			nextXWall[i] = bb.getShort();
		for(int i = 0; i < kMaxXSectors; i++) 
			nextXSector[i] = bb.getShort();
		
		for(int i = 0; i < kMaxXSprites; i++) 
			xsprite[i].free();

		buf = new byte[XSPRITE.sizeof];
		for(int i = 0; i < kMaxSprites; i++) 
		{
			if ( sprite[i].statnum < kStatFree )
		    {
		    	if ( sprite[i].extra > 0 ) {
					bb.get(buf);
					xsprite[sprite[i].extra].init(buf);
					xsprite[sprite[i].extra].reference = BitHandler.bsread(buf, 0, 0, 13);
		    	}
		    }
		}
		
		for(int i = 0; i < kMaxXWalls; i++) 
			xwall[i].free();

		buf = new byte[XWALL.sizeof];
		for(int i = 0; i < numwalls; i++) 
		{
	    	if ( wall[i].extra > 0 ) {
				bb.get(buf);
				xwall[wall[i].extra].init(buf);
				xwall[wall[i].extra].reference = BitHandler.bsread(buf, 0, 0, 13);
	    	}
		}
		
		for(int i = 0; i < kMaxXSectors; i++) 
			xsector[i].free();
		
		buf = new byte[XSECTOR.sizeof];
		for(int i = 0; i < numsectors; i++) 
		{
	    	if ( sector[i].extra > 0 ) {
				bb.get(buf);
				xsector[sector[i].extra].init(buf);
				xsector[sector[i].extra].reference = BitHandler.bsread(buf, 0, 0, 13);
	    	}
		}
		
		Arrays.fill(sprXVel, 0);
		Arrays.fill(sprYVel, 0);
		Arrays.fill(sprZVel, 0);
		
		for(int i = 0; i < numsprites; i++) {
			int xvel = bb.getInt();
			if(i >= kMaxSprites) 
				continue;
			sprXVel[i] = xvel;
		}
		for(int i = 0; i < numsprites; i++) {
			int yvel = bb.getInt();
			if(i >= kMaxSprites) 
				continue;
			sprYVel[i] = yvel;
		}
		for(int i = 0; i < numsprites; i++) {
			int zvel = bb.getInt();
			if(i >= kMaxSprites) 
				continue;
			sprZVel[i] = zvel;
		}
		
		bb.getInt(); //dword_19AE34
		bb.getInt(); //dword_19AE38
		
		gSkyCount = bb.getInt();
		gFogMode = bb.get() == 1;
		
		if(numsprites >= kMaxSprites)
			numsprites = (short) (kMaxSprites - 1);
		
		gNoClip = false;
		gFullMap = false;
		
		if(nVersion < gdxSave)
		{
			safeGameInfo.nEnemyDamage = safeGameInfo.nDifficulty;
			safeGameInfo.nEnemyQuantity = safeGameInfo.nDifficulty;
			safeGameInfo.nPitchforkOnly = false;
			gInfiniteAmmo = false;
		}
	}
	
	public void LoadGameInfo(ResourceData bb)
	{
		safeGameInfo.nGameType = bb.get() & 0xFF;
		safeGameInfo.nDifficulty = bb.get() & 0xFF;
		safeGameInfo.nEpisode = bb.getInt();       
		safeGameInfo.nLevel = bb.getInt();  

		byte[] buf = new byte[144];
		bb.get(buf);
		String name = new String(buf).trim();
		name = Strhandler.toLowerCase(name);
		if(FileUtils.isExtension(name, "map"))
			name = name.substring(0, name.lastIndexOf('.'));
		safeGameInfo.zLevelName = name;
		bb.get(buf);
		safeGameInfo.zLevelSong = new String(buf).trim();
		safeGameInfo.nTrackNumber = bb.getInt();
		buf = new byte[16];
		bb.get(buf); //szSaveGameName - gamexxxx.sav  
		bb.get(buf); 
		bb.getShort(); //nSaveGameSlot  
		bb.getInt(); //picEntry 

		safeGameInfo.uMapCRC = bb.getInt(); //uMapCRC
		safeGameInfo.nMonsterSettings = bb.get() & 0xFF;             
		safeGameInfo.uGameFlags = bb.getInt();                
		safeGameInfo.uNetGameFlags = bb.getInt();      
		safeGameInfo.nWeaponSettings = bb.get() & 0xFF;        
		safeGameInfo.nItemSettings   = bb.get() & 0xFF;        
		safeGameInfo.nRespawnSettings = bb.get() & 0xFF;     
		safeGameInfo.nTeamSettings = bb.get() & 0xFF;     
		safeGameInfo.nMonsterRespawnTime = bb.getInt();               
		safeGameInfo.nWeaponRespawnTime  = bb.getInt();                   
		safeGameInfo.nItemRespawnTime  = bb.getInt();                     
		safeGameInfo.nSpecialRespawnTime  = bb.getInt();     
	}

	public void DudesLoad(ResourceData bb)
	{
		for(int i = 0; i < kMaxXSprites; i++) {
			cumulDamage[i] = 0;
			bb.getInt();
		}
		for(int i = 0; i < kMaxXSprites; i++) 
			gDudeSlope[i] = bb.getInt();
	}
	
	public void WarpLoad(ResourceData bb)
	{
		for(int i = 0; i < kMaxPlayers; i++) 
		{
			gStartZone[i].x = bb.getInt();
			gStartZone[i].y = bb.getInt();
			gStartZone[i].z = bb.getInt();
			gStartZone[i].sector = bb.getShort();
			gStartZone[i].angle = bb.getShort();
		}
		for(int i = 0; i < kMaxSectors; i++) 
			gUpperLink[i] = bb.getShort();
		for(int i = 0; i < kMaxSectors; i++) 
			gLowerLink[i] = bb.getShort();
	}
	
	public void MirrorLoad(ResourceData bb)
	{
		mirrorcnt = bb.getInt();
		MirrorSector = bb.getInt();
		for(int i = 0; i < MAXMIRRORS; i++)
		{
			MirrorType[i] = bb.getShort();
			bb.getShort(); //pad
			MirrorLower[i] = bb.getInt();
			MirrorX[i] = bb.getInt();
			MirrorY[i] = bb.getInt();
			MirrorZ[i] = bb.getInt();
			MirrorUpper[i] = bb.getInt();
		}
		for(int i = 0; i < 4; i++) 
			MirrorWall[i] = bb.getInt();
	}
	
	public void SeqLoad(ResourceData bb, int nVersion) throws FileNotFoundException
	{
		for(int i = 0; i < kMaxXWalls; i++) {
			siWall[i].load(bb);
		}
		for(int i = 0; i < kMaxXWalls; i++) {
			siMasked[i].load(bb);
		}
		for(int i = 0; i < kMaxXSectors; i++) { 
			siCeiling[i].load(bb);
		}
		for(int i = 0; i < kMaxXSectors; i++) {
			siFloor[i].load(bb);
		}
		for(int i = 0; i < kMaxXSprites; i++) {
			siSprite[i].load(bb);
		}
		
		int len = kMaxSequences;
		if(nVersion < gdxSave + 2) len = 1024;
		Arrays.fill(actListType, (byte) 0);
		Arrays.fill(actListIndex, (short) 0);
		
		for(int i = 0; i < len; i++) {
			actListType[i] = bb.get();
			actListIndex[i] = bb.getShort();
		}

		activeCount = bb.getInt();
		for(int i = 0; i < activeCount; i++)
		{
			SeqInst pInst = GetInstance(actListType[i], actListIndex[i]);
			if(pInst.isPlaying() && !BuildGdx.cache.contains(pInst.getSeqIndex(), "SEQ"))
				throw new FileNotFoundException("hSeq != null, id=" + pInst.getSeqIndex() + " \n\rWrong Blood.RFF version or file corrupt!");
		}
	}
	
	public SeqInst GetInstance( int type, int nXIndex )
	{
		switch ( type )
		{
			case SS_WALL:
				if(nXIndex <= 0 || nXIndex >= kMaxXWalls) 
					return null;
				return siWall[nXIndex];

			case SS_CEILING:
				if(nXIndex <= 0 || nXIndex >= kMaxXSectors) 
					return null;
				return siCeiling[nXIndex];

			case SS_FLOOR:
				if(nXIndex <= 0 || nXIndex >= kMaxXSectors) 
					return null;
				return siFloor[nXIndex];

			case SS_SPRITE:
				if(nXIndex <= 0 || nXIndex >= kMaxXSprites) 
					return null;
				return siSprite[nXIndex];

			case SS_MASKED:
				if(nXIndex <= 0 || nXIndex >= kMaxXWalls) 
					return null;
				return siMasked[nXIndex];

		}

		return null;
	}

	public void EventLoad(ResourceData bb)
	{
		for(int i = 0; i < 1025; i++)
		{
			qEventPriority[i] = bb.getInt();
			qEventEvent[i] = bb.getInt();
		}
		fNodeCount = bb.getInt();

		for(int i = 0; i < kMaxChannels; i++) {
			int data = bb.getInt();
			rxBucketIndex[i] = getIndex(data);
			rxBucketType[i] = getType(data);
		}
		
		for(int i = 0; i <= kMaxID; i++) 
			bucketHead[i] = bb.getShort();
	}
	
	public void TriggersLoad(ResourceData bb)
	{
		gBusyCount = bb.getInt();
		for(int i = 0; i < kMaxBusyArray; i++) {
			gBusy[i].nIndex = bb.getInt();
			gBusy[i].nDelta = bb.getInt();
			gBusy[i].nBusy = bb.getInt();
			gBusy[i].busyProc = bb.get() & 0xFF;
		}
		for(int i = 0; i < kMaxSectors; i++)
			secPath[i] = bb.getInt();
	}
	
	public void PlayersLoad(ResourceData bb, int nVersion)
	{
		for(int i = 0; i < kMaxPlayers; i++)
			nTeamCount[i] = bb.getInt();
		gNetPlayers = bb.getInt();

		for(int i = 0; i < gNetPlayers - 1; i++) 
			connectpoint2[i] = (short) (i+1);
		connectpoint2[gNetPlayers-1] = -1;
		
		byte[] plname = new byte[15];
		for(int i = 0; i < kMaxPlayers; i++)
		{
			autoaim[i] = bb.get();
			if(nVersion >= gdxSave + 2)
				slopetilt[i] = bb.get();
			skill[i] = bb.get();
			bb.get(plname);
			name[i] = new String(plname).trim();
		}
		
		byte[] input = new byte[INPUT.sizeof(nVersion)];
		for(int i = 0; i < kMaxPlayers; i++)
		{
			bb.getInt(); //sprite
			bb.getInt(); //xsprite
			bb.getInt(); //dudeinfo
			bb.get(input);
			safePlayer[i].pInput = new INPUT(input, nVersion);
			safePlayer[i].set(bb, nVersion);
		}
	}
	
	public void ActorsLoad(ResourceData bb)
	{
		for(int i = 0; i < kMaxXSprites; i++) {
			gSpriteHit[i].moveHit = bb.getInt();
			gSpriteHit[i].ceilHit = bb.getInt();
			gSpriteHit[i].floorHit = bb.getInt();
		}
		
		for(int i = 0; i < kMaxSectors; i++) 
			gSectorExp[i] = bb.getShort();
		for(int i = 0; i < kMaxXWalls; i++) 
			gWallExp[i] = bb.getShort();

		gPostCount = bb.getInt();
		for(int i = 0; i < kMaxSprites; i++) {
			gPost[i].nSprite = bb.getShort();
			gPost[i].nStatus = bb.getShort();
		}
	}
	
	public void GameInfoLoad(ResourceData bb)
	{
		gNextMap = bb.getInt();
		bb.getShort(); //unk_111330 unk_133FB4
		LoadGameInfo(bb);      
		bb.get(); //gPlaygame
		bb.get(); //byte_1A9C92
	}
	
	public void StatsLoad(ResourceData bb)
	{
		totalSecrets = bb.getInt();
		foundSecret = bb.getInt();
		superSecrets = bb.getInt();
		totalKills = bb.getInt();
		kills = bb.getInt();
	}
	
	public void ScreenLoad(ResourceData bb)
	{
		bb.getInt(); // unk_128C08
		byte[] buf = new byte[256];
		bb.get(buf); //unk_128C0C
		bb.getShort(); // unk_128E10
		bb.getShort(); // unk_128E12
		bb.getInt(); //gScreenTilt
		deliriumTilt = bb.getInt();
		deliriumTurn = bb.getInt();
		deliriumPitch = bb.getInt();
	}
	
	public void LoadUserEpisodeInfo(ResourceData bb)
	{
		gUserEpisode = bb.get() == 1;
		if(gUserEpisode) {
			byte[] buf = new byte[144];
			bb.get(buf);
			addonFileName = Strhandler.toLowerCase(new String(buf).trim());
		}
	}

	public void LoadGDXBlock(ResourceData bb)
	{
		LoadUserEpisodeInfo(bb);
		
		byte[] data = new byte[SAVEGDXDATA];
		bb.get(data);
		int pos = 0;
		
		safeGameInfo.nEnemyDamage = data[pos++];
		safeGameInfo.nEnemyQuantity = data[pos++];
		safeGameInfo.nDifficulty = data[pos++];
		safeGameInfo.nPitchforkOnly = data[pos++] == 1;
		gInfiniteAmmo = data[pos++] == 1;
	}
}
