// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Fonts;

import static ru.m210projects.Build.Engine.tilesizx;
import static ru.m210projects.Build.Engine.tilesizy;
import static ru.m210projects.Build.Engine.waloff;

import java.util.Arrays;

import ru.m210projects.Blood.Types.CHARINFO;
import ru.m210projects.Build.FileHandle.Resource.ResourceData;

public class QFont {

	public byte charSpace;
	public byte width;
	public byte height;
	
	private CHARINFO[] info; // characters info
	private byte[] data; // image data
	private byte baseline;
	
	public QFont(ResourceData bb)
	{
    	byte[] buf = new byte[12];
    	bb.get(buf, 0, 4);
    	
    	/* String signature = new String(buf, 0, 4); */
    	/* short version = */ bb.getShort();
    	short type = bb.getShort();
    	int totalsize = bb.getInt();
    	/* byte startChar = */ bb.get();
    	/* byte endChar = */ bb.get();
    	/* byte blending = */ bb.get();
    	baseline = bb.get();
    	/*byte tcolor = */ bb.get();
    	charSpace = bb.get();
    	width = bb.get();
    	height = bb.get();
    	bb.get(buf, 0, 12); // filler to 32 bytes boundary  
    	info = new CHARINFO[256];
    	for(int i = 0; i < 256; i++)
    		info[i] = new CHARINFO(bb);
    	
    	/* PATCH: "FONTBLOD.QFN" font fix for Russian Blood Lenin version */
		if ((width == 12) && (height == 5) && (totalsize == 40674) && (type != 0)
				&& (baseline == 0) && (charSpace == 0)) {
			baseline = 8;
			height = 21;
		}
		/* PATCH: "KFONT7.QFN" font fix for Russian Blood Lenin version */
		if ((width == 6) && (height == 8) && (totalsize == 7566) && (type != 0)
				&& (baseline == 6) && (charSpace == 1)) {
			baseline = 8;
		}
		/* PATCH: "pQFN2O.QFN" font fix for Russian Blood Lenin version */
		if ((width == 15) && (height == 15) && (totalsize == 16866) && (type != 0)
				&& (baseline == 15) && (charSpace == -1)) {
			baseline = 8;
		}
 
    	int len = bb.capacity() - bb.position();
    	this.data = new byte[len];
    	bb.get(this.data);
	}
	
	public void buildChar(int nBase, int nChar) {
		CHARINFO pInfo = info[nChar + 32];
		short sizeX = (short) (pInfo.cols & 0xFF);
		short sizeY = (short) (pInfo.rows & 0xFF);

		int nSize = sizeX * sizeY;

		if (nChar != 0 && nSize == 0)
			return;

		int nTile = nChar + nBase;

		if (nChar == 0) // space
			sizeX = (short) (width / 2);
		tilesizx[nTile] = sizeX;
		tilesizy[nTile] = height;
		waloff[nTile] = new byte[tilesizx[nTile] * tilesizy[nTile]];

		Arrays.fill(waloff[nTile], (byte) 0xFF);
		int voffset = baseline + pInfo.voffset;
		for (int y = 0; y < sizeY; y++) {
			if (y + voffset >= tilesizy[nTile])
				break;
			for (int x = 0; x < sizeX; x++) {
				if (voffset + y + x * tilesizy[nTile] >= 0)
					waloff[nTile][voffset + y + x * tilesizy[nTile]] = data[pInfo.offset + y + x * sizeY];
			}
		}
	}

}
