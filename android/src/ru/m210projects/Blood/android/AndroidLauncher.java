package ru.m210projects.Blood.android;

import java.io.File;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import android.os.Bundle;
import android.os.Environment;
import ru.m210projects.Blood.Config;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Architecture.BuildGdx;
import ru.m210projects.Build.Audio.BuildAudio;
import ru.m210projects.Build.FileHandle.Cache1D;
import ru.m210projects.Build.FileHandle.Compat;
import ru.m210projects.Build.FileHandle.Compat.Path;
import ru.m210projects.Build.android.AndroidGL10;
import ru.m210projects.Build.android.AndroidMessage;

public class AndroidLauncher extends AndroidApplication {
	
	public static final String appname = "BloodGDX";
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		String filepath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "BloodGDX" + File.separator;
		BuildGdx.compat = new Compat(filepath, filepath);
		BuildGdx.cache = new Cache1D(BuildGdx.compat);

		Config BloodCFG = new Config(Path.Game.getPath(), appname + ".ini");

		initialize(new Main(BloodCFG, appname, Main.sversion, false), config);
		Gdx.gl = new AndroidGL10();
		
//		BuildGdx.app = Gdx.app;
		BuildGdx.files = Gdx.files;
		BuildGdx.net = Gdx.net;
		BuildGdx.audio = new BuildAudio();
		BuildGdx.message = new AndroidMessage(this);
	}
}
